﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public class AiPathController : MonoBehaviour
{
    public AstarPath _path;
    public GridGenerator gridGenerator;
    // Start is called before the first frame update
    void Start()
    {
        _path = GetComponent<AstarPath>();
        Invoke(nameof(SetPathSize), 2f);
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void SetPathSize()
    {
        GridGraph graph = _path.data.gridGraph;
        var screenSize = gridGenerator.screenSizeInworldSpace;
        int width =(int) Mathf.Abs(Mathf.CeilToInt( screenSize.x))*2;
        int depth = (int)Mathf.Abs(Mathf.CeilToInt(screenSize.y)) * 2;
        Debug.Log("W==" + width);
        Debug.Log("D==" + depth);
        graph.SetDimensions(width, depth, 1);
        _path.Scan();


    }
    public void UpdatePath()
    {
        _path.Scan();
    }
}
