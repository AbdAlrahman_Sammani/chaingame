﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;

public class ParticleController : MonoBehaviour
{
    public List<GameObject> gameFX = new List<GameObject>();
    private void OnEnable()
    {
        EventsManager.OnPlayParticleAction += PlayMyParicles;
    }
    private void OnDisable()
    {
        EventsManager.OnPlayParticleAction -= PlayMyParicles;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayMyParicles(EventsManager.GameEventsArgs args)
    {
        Debug.Log("Particle Play");
        Instantiate(gameFX[args._particleID], args._particlePosition.position, Quaternion.identity);
       // gameFX[id].gameObject.transform.position = pos.position;
      //  gameFX[id].SetActive(true);
    }
}
