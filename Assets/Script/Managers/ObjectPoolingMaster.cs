﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPoolingMaster : MonoBehaviour
{
    Dictionary<int, Queue<GameObject>> poolDictionary = new Dictionary<int, Queue<GameObject>>();

    public GameObject objectBoolParent;
    /// <summary>
    /// singltone to access this class
    /// </summary>
    static ObjectPoolingMaster _instance;
    public static ObjectPoolingMaster instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ObjectPoolingMaster>();
            }
            return _instance;
        }

    }
    private void Start()
    {
    }

    /// <param name="prefab"></param>
    /// <param name="poolSize"></param>
    public void Createpool(GameObject prefab, int poolSize)
    {
        int poolKey = prefab.GetInstanceID();
        //Check if the pool is exist
        if (!poolDictionary.ContainsKey(poolKey)) // if not so create a new pool
        {
            poolDictionary.Add(poolKey, new Queue<GameObject>());
        }
        // instantiate a prefabs as pool size 
        for (int i = 0; i < poolSize; i++)
        {
            GameObject newGameObject = Instantiate(prefab) as GameObject;
            newGameObject.SetActive(false);
            poolDictionary[poolKey].Enqueue(newGameObject);///add the newGameObject to the Queue
            //newGameObject.transform.parent = this.gameObject.transform;
            newGameObject.transform.SetParent(objectBoolParent.transform);

        }


    }
    public void UpdatePool(GameObject prefab, int newsize)
    {
        int poolKey = prefab.GetInstanceID();
        if (poolDictionary.ContainsKey(poolKey))
        {
            if (poolDictionary[poolKey].Count != newsize)
            {
                int newpoolSize = newsize - poolDictionary[poolKey].Count;
                Debug.Log("Added to pool = " + newpoolSize);
                for (int i = 0; i < newpoolSize; i++)
                {
                    GameObject newGameObject = Instantiate(prefab) as GameObject;
                    newGameObject.SetActive(false);
                    poolDictionary[poolKey].Enqueue(newGameObject);///add the newGameObject to the Queue
                    newGameObject.transform.parent = this.gameObject.transform;
                }
            }
        }
    }


    public void ReuseGameObject(GameObject prefab, Vector3 position, Quaternion quaternion, out GameObject obs)
    {

        int poolKey = prefab.GetInstanceID();
        if (poolDictionary.ContainsKey(poolKey))
        {
            GameObject object_toReuse = poolDictionary[poolKey].Dequeue(); /// give me the object and put it in the last of queue
            poolDictionary[poolKey].Enqueue(object_toReuse);
            obs = object_toReuse;
            if (object_toReuse.activeSelf) return;
            object_toReuse.SetActive(true);
            object_toReuse.transform.position = position;
            object_toReuse.transform.rotation = quaternion;

        }
        else { obs = null; }
    }
    public void DisactiveObject(GameObject prefab)
    {

        int poolKey = prefab.GetInstanceID();
        if (poolDictionary.ContainsKey(poolKey))
        {

            poolDictionary[poolKey].All((go) => {Destroy(go); return true; });
            poolDictionary.Remove(poolKey);
          //  GameObject object_toReuse = poolDictionary[poolKey].Dequeue(); /// give me the object and put it in the last of queue
            //  poolDictionary[poolKey].Enqueue(object_toReuse);
            //  if (object_toReuse.activeSelf) return;
            //object_toReuse.SetActive(false);
           // Destroy(object_toReuse);
            Debug.Log("DES");


        }
    }
}
