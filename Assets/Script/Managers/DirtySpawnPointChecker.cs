﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtySpawnPointChecker : MonoBehaviour {
   public static bool isDirty;
    public Vector2 spawnPosition;
	// Use this for initialization
	void Start () {
        isDirty = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void UpdateSpawnArea()
    {
         spawnPosition=new  Vector2(Random.Range(-GameManager.spawningAreaWidth, GameManager.spawningAreaWidth), Random.Range(GameManager.spawningAreaMinHeight, GameManager.spawningAreaHeight));
         transform.position = spawnPosition;
       
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag=="Enemy")
        {
            isDirty = true;
            UpdateSpawnArea();
        }
        else
        {
            isDirty = false;
        }
        
      //  Debug.Log("Hit Somthing"+ collision.tag+ "isDirty"+isDirty);
    }

}
