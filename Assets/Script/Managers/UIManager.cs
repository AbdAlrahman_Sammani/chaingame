﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSystem;


public class UIManager : MonoBehaviour {
    public Text score, fuel;
    public GameObject mainMenuPanel;
    public GameObject levelSelectPanel;
    public List<Button> levelsSelectButtonList;
    public Button startButton;
    private void OnEnable()
    {
        EventsManager.OnStartGameAction += OnStartGame;
        EventsManager.OnWinLevelActions += OnWinLevel;
        EventsManager.OnLoseLevelActions += OnLoseLevel;
    }
    private void OnDisable()
    {
        EventsManager.OnStartGameAction -= OnStartGame;
        EventsManager.OnWinLevelActions -= OnWinLevel;
        EventsManager.OnLoseLevelActions -= OnLoseLevel;
    }
    // Use this for initialization
    void Start () 
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        startButton.onClick.AddListener(() => OnStartButtonClick());
        for (int i = 0; i < levelsSelectButtonList.Count; i++)
        {
            levelsSelectButtonList[i].onClick.AddListener(() => OnStartLevelButtonClick(i));
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void UpdateFuelUI()
    {

    }
    public void UpdateScoreUI()
    {

    }
    public void OnStartLevelButtonClick(int id)
    {
    }
    public void OnStartGame(EventsManager.GameEventsArgs args)
    {
        //  mainMenuPanel.SetActive(false);
        //  levelSelectPanel.SetActive(false);
        ShowPage(2);

    }
    public void OnStartButtonClick()
    {
        ShowPage(1);
    }


    public void ShowMainMenu(EventsManager.GameEventsArgs args)
    {
        mainMenuPanel.SetActive(!args._startState);
    }
    
    public void OnWinLevel(EventsManager.GameEventsArgs args)
    {
      //  mainMenuPanel.SetActive(true);
        ShowPage(3);
       // levelSelectPanel.SetActive(true);

    }
    public void OnLoseLevel(EventsManager.GameEventsArgs args)
    {
       // mainMenuPanel.SetActive(true);
        ShowPage(4);

        // levelSelectPanel.SetActive(true);
    }
  
    public Menu menu;
    public void ShowPage(int id)
    {
        Menu _menu = FindObjectOfType<Menu>();
        menu = _menu;
        _menu.ShowPage(id);
    }

}
