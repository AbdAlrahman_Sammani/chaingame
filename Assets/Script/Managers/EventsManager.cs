﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

namespace GameSystem {
    public class EventsManager
    {
        public class GameEventsArgs
        {
            public GameObject _changeActiveStateGameObject;
            public float _changeActiveStateTime;
            public bool _changeActiveState_bool;

            public GameObject _powerupGameObject;

            public int _soundID;

            public float _powerupTime;
            public int _hammerSize;

            public bool _startState;

            public int _particleID;

            public Transform _particlePosition;

            public string _InGameText;
            public string _ScoreUpdateValue;

            public PowerUps _powerupType;

            public PowerUpsManager.PowerupManagerModel _powerUpManagerModel;
            public EnemyManager.EnemeyManagerModel _EnemyManagerModel;
            public TheBase.BaseHealthModel _healthModel;
            public TheBase.BaseShapeModel _shapeModel;
        }


        #region input Events
        public static Action OnTouchScreenAction;
       
        public Action OnGetoutFromScreenAction;
        #endregion 
      
        #region GameEvents
        public static Action<GameEventsArgs> OnChangeActiveStateAction;
       

        public static Action<GameEventsArgs> OnGameOverAction;
       

        public Action OnRestartGameAction;
       

        public static Action OnHitBaseAction;
        

        public static Action ChageBackgroundAction;
        
        /// <summary>
        /// Event to Create a PowerUp
        /// </summary>
        public Action<GameEventsArgs> OnPowerupInstantiate;
       


        /// <summary>
        /// Data Update
        /// </summary>
        public static Action OnDataUpdateAction;
      

        public static Action <GameEventsArgs> OnSoundPlayCallAction;
       

        #endregion
        #region PowerUps

        public static Action<GameEventsArgs> OnPowerupCallAction;


        public static Action<GameEventsArgs> OnChangeHammerSizeAction;
        

        #endregion
        #region Enemies
        public static Action OnEnemeyHitAction;
        

        public static Action OnEnemeyDoubleHitAction;
       
        #endregion
        #region StartGame
        public static Action<GameEventsArgs> OnStartGameAction;
        
        #endregion
        #region perticles
        public static Action<GameEventsArgs> OnPlayParticleAction;
       
        #endregion
        #region UI
        public static Action<GameEventsArgs> ShowInGameTextAction;
        

        public static Action<GameEventsArgs> ScoreUpdaterAction;
        public delegate void ScoreUpdater(string value);
        public static event ScoreUpdater _onScoreUpdate;
        public static void CallOnScoreUpdate(string value)
        {
            _onScoreUpdate.Invoke(value);
        }

        public static Action<GameEventsArgs> OnStartNewLevelActions;

        public static Action<GameEventsArgs> OnWinLevelActions;
        public static Action<GameEventsArgs> OnLoseLevelActions;

        #endregion
    }
}

