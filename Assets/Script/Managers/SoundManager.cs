﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public class SoundManager : MonoBehaviour {
    public AudioClip[] allSounds;
     AudioSource audioSource;
    // Use this for initialization
    private void OnEnable()
    {
        EventsManager.OnSoundPlayCallAction += PlaySound;
    }
     void OnDisable()
    {
        EventsManager.OnSoundPlayCallAction -= PlaySound;

    }
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
       
    }
    public  void PlaySound(EventsManager.GameEventsArgs args)
    {
       // Debug.Log("sond is play");
        audioSource.PlayOneShot(allSounds[args._soundID]);
    }
}
