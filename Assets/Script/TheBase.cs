﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public enum BaseState { _3layer, _2Layer, _1Layer, _Icon };
public class TheBase : MonoBehaviour
{
    [System.Serializable]
    public class BaseShapeModel
    {
        public Sprite _1Sprite, _2Sprite, _3Sprite, _iconSprite;
    }
    [System.Serializable]
    public class BaseHealthModel
    {
        public int layer1Satmina, layer2Satmina, layer3Satmina, iconSatmina;
    }
    
    public BaseShapeModel _baseshapeModel;
    public BaseHealthModel _baseHealthModel;

    public BaseState baseState = new BaseState();

    public GameObject _1layer, _2layer, _3layer;
    public GameObject theIcon;


    private int layer3Health, layer2Health, layer1Health, iconHealth;

    private void OnEnable()
    {
        EventsManager.OnHitBaseAction += BaseHited;
        EventsManager.OnStartNewLevelActions += SetBaseHealth;
    }
    private void OnDisable()
    {
        EventsManager.OnHitBaseAction -= BaseHited;
        EventsManager.OnStartNewLevelActions -= SetBaseHealth;

    }
    private void Start()
    {
        ResetDefault();
        
    }
    

    public void OnTriggerEnter2D(Collider2D other)
    {
        /*Debug.Log(other.gameObject.tag);
        if ((other.gameObject.tag != "Axis") || (other.gameObject.tag != "Hammer"))
        {
            {
                switch (baseState)
                {
                    case BaseState._3layer:
                        if (layer3Health != 30)
                        {
                            layer3Health += 10;
                        }
                        else
                        {
                            _3layer.SetActive(false);
                            baseState = BaseState._2Layer;
                        }
                        break;
                    case BaseState._2Layer:
                        if (layer2Health != 40)
                        {
                            layer2Health += 10;
                        }
                        else
                        {
                            // Destroy(_2layer);
                            _2layer.SetActive(false);
                            baseState = BaseState._1Layer;
                        }
                        break;
                    case BaseState._1Layer:
                        if (layer1Health != 50)
                        {
                            layer1Health += 50;
                        }
                        else
                        {
                            // Destroy(_1layer);
                            _1layer.SetActive(true);
                            baseState = BaseState._Icon;
                        }
                        break;
                    case BaseState._Icon:
                        if (iconHealth != 60)
                        {
                            iconHealth += 10;
                        }
                        else
                        {
                            Debug.Log("game over");
                            EventsManager._OnGameOverCall();
                            ResetDefault();
                        }
                        break;
                    default:
                        break;

                }
                EventsManager._onBaseHitCall();
                
            }
        }*/
    }
    public void BaseHited()
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._soundID = 4;
        EventsManager.OnSoundPlayCallAction(args);
        ManageBaseHealth();
    }
    public void ResetDefault()
    {
        _1layer.SetActive(true);
        _2layer.SetActive(true);
        _3layer.SetActive(true);
        layer1Health = 0;
        layer2Health = 0;
        layer3Health = 0;
        iconHealth = 0;
        baseState = BaseState._3layer;
    }





    public void ManageBaseHealth()
    {
        // print("Manage Base Hit Called");
        switch (baseState)
        {
            case BaseState._3layer:
                if (layer3Health != _baseHealthModel.layer3Satmina)
                {
                    layer3Health += 10;
                }
                else
                {
                    _3layer.SetActive(false);
                    baseState = BaseState._2Layer;
                }
                break;
            case BaseState._2Layer:
                if (layer2Health != _baseHealthModel.layer2Satmina)
                {
                    layer2Health += 10;
                }
                else
                {
                    // Destroy(_2layer);
                    _2layer.SetActive(false);
                    baseState = BaseState._1Layer;
                }
                break;
            case BaseState._1Layer:
                if (layer1Health != _baseHealthModel.layer1Satmina)
                {
                    layer1Health += 10;
                }
                else
                {
                    // Destroy(_1layer);
                    _1layer.SetActive(false);
                    baseState = BaseState._Icon;
                }
                break;
            case BaseState._Icon:
                if (iconHealth != _baseHealthModel.iconSatmina)
                {
                    iconHealth += 10;
                }
                else
                {
                    Debug.Log("game over");
                    EventsManager.OnLoseLevelActions.Invoke(new EventsManager.GameEventsArgs());
                    ResetDefault();
                }
                break;
            default:
                break;

        }
    }
    public void SetBaseHealth(EventsManager.GameEventsArgs args)
    {
        _baseHealthModel = args._healthModel;
    }
    public void SetBaseShape(BaseShapeModel _shapeModel)
    {
        _baseshapeModel = _shapeModel;
        _1layer.GetComponent<SpriteRenderer>().sprite = _shapeModel._1Sprite;
        _2layer.GetComponent<SpriteRenderer>().sprite = _shapeModel._2Sprite;
        _3layer.GetComponent<SpriteRenderer>().sprite = _shapeModel._3Sprite;
        theIcon.GetComponent<SpriteRenderer>().sprite = _shapeModel._iconSprite;


    }
   
    /*
     EventsManager._onBaseHitCall();

 }*/

}

