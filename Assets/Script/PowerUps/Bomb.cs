﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {
    public GameObject explosion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if ((GetComponent<SpriteRenderer>().color == Color.red)&&(collision.CompareTag("Enemy")))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            collision.gameObject.GetComponent<Rigidbody2D>().mass = .1f;
            explosion.SetActive(false);
            explosion.SetActive(true);
            collision.gameObject.GetComponent<SpriteRenderer>().material.color = Color.red;
            Destroy(collision.gameObject, 1f);
            ScoreManager.Score++;
            Destroy(gameObject, 2);
        }
        }
}
