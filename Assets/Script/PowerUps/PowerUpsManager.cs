﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public class PowerUpsManager : MonoBehaviour
{
    [System.Serializable]
    public class PowerupManagerModel
    {
        public GameObject[] powerUpsGameObjets;
        public float timeToGenerate;
        public float timeToDisapper;
        public int powerupsCount;
        public int timeToinstantiateFuel;
    }
    public PowerupManagerModel _powerupManagerModel;

    //public Transform spawnPoints;

    private void OnEnable()
    {
        //   EventsManager._onRefillFuel += ReFillFuel;
        EventsManager.OnStartNewLevelActions += StartPowerupManager;
        EventsManager.OnPowerupCallAction += CallPowerUp;
        EventsManager.OnLoseLevelActions += OnLoseLevel;

    }
    private void OnDisable()
    {
        EventsManager.OnPowerupCallAction -= CallPowerUp;
        EventsManager.OnStartNewLevelActions -= StartPowerupManager;
        EventsManager.OnWinLevelActions -= OnWinLevel;
        EventsManager.OnLoseLevelActions -= OnLoseLevel;




    }
    private void Awake()
    {

    }
    void Update()
    {
        if (!IsInvoking())
        {

        }
    }
    public void InvokePowerupInstantiator()
    {
        PowerUpInsantiator(_powerupManagerModel.powerUpsGameObjets[Random.Range(0, _powerupManagerModel.powerUpsGameObjets.Length)]);

    }
    public void InvokeFuel()
    {
        PowerUpInsantiator(_powerupManagerModel.powerUpsGameObjets[0]);
    }
    protected virtual void PowerUpEffect()
    {

    }
    protected virtual void PowerUpMovments()
    {

    }

    public void PowerUpInsantiator(GameObject ob)
    {
        GameObject instantiatedObject;
        //   ChangeSpawnPointLocation();
        GridGenerator gridGenerator = GameManager.Instance.gameObject.GetComponent<GridGenerator>();

        var unusedList = GameManager.Instance.unUsedListPos;
        var spawnPoint = gridGenerator.GetRandomPosition3(unusedList);

        var pos = gridGenerator.GetRandomPosition3(unusedList);

        ObjectPoolingMaster.instance.ReuseGameObject(ob, pos.position, Quaternion.identity, out instantiatedObject);
        pos.state = true;
        _powerupManagerModel.powerupsCount++;
        _powerupManagerModel.timeToDisapper = _powerupManagerModel.timeToGenerate;
        EventsManager.GameEventsArgs eventsArgs = new EventsManager.GameEventsArgs();
        eventsArgs._changeActiveStateGameObject = instantiatedObject;
        eventsArgs._changeActiveStateTime = 5f;
        eventsArgs._changeActiveState_bool = false;
        //  EventsManager.OnChangeActiveStateAction(instantiatedObject, 5, false);
        // EventsManager.OnChangeActiveStateAction.Invoke(eventsArgs); //Remove The object After Time End
        StartCoroutine(RemovePowerup(instantiatedObject, 5));
    }


    float spawnx, spawny;
    public void ChangeSpawnPointLocation()
    {
        spawnx = Random.Range(-GameManager.spawningAreaWidth, GameManager.spawningAreaWidth);
        spawny = Random.Range(GameManager.spawningAreaMinHeight, GameManager.spawningAreaHeight);
        //  spawnPoints.transform.position = new Vector2(spawnx, spawny);
    }
    public IEnumerator RemovePowerup(GameObject ob, float _time)
    {
        yield return new WaitForSeconds(_time);
        ob.SetActive(false);
    }
    public void ReFillFuel(int value)
    {
        DataContainer.instance.fuelAmount = value;
    }
    public void HammerLargePowerUpHandler()
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._hammerSize = 2;
        args._powerupTime = 4;
        EventsManager.OnChangeHammerSizeAction.Invoke(args);
    }
    public void MagnetPowerUpHandler()
    {
        //isMagnetActive = true;
        //yield return new WaitForSecondsRealtime(10);
        //EnemyAI.isMagnetActive = false;
    }
    public IEnumerator SlowPowerUpHandler()
    {
        Time.timeScale = .5f;
        yield return new WaitForSecondsRealtime(10);
        Time.timeScale = 1;
    }
    public void WavePowerupHandler()
    {
        foreach (var item in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            item.SetActive(false);
        }
    }
    public void BombPowerupHandler()
    {

    }
    public void CallPowerUp(EventsManager.GameEventsArgs args)
    {

        switch (args._powerupType)
        {
            case PowerUps.Fuel:
                ReFillFuel(100);
                break;
            case PowerUps.Magnet:
                MagnetPowerUpHandler();
                break;
            case PowerUps.Bomb:
                BombPowerupHandler();
                break;
            case PowerUps.Wave:
                WavePowerupHandler();
                break;
            case PowerUps.Slow:
                StartCoroutine("SlowPowerUpHandler");
                break;
            case PowerUps.Large:
                HammerLargePowerUpHandler();
                break;
            default:
                break;
        }
    }

    public void StartPowerupManager(EventsManager.GameEventsArgs args)
    {
        _powerupManagerModel = args._powerUpManagerModel;
        foreach (var item in _powerupManagerModel.powerUpsGameObjets)
        {
            ObjectPoolingMaster.instance.Createpool(item, 1);
        }
        InvokeAllMethods();
    }
    public void InvokeAllMethods()
    {
        CancelInvoke();
        InvokeRepeating(nameof(InvokePowerupInstantiator),_powerupManagerModel.timeToGenerate, _powerupManagerModel.timeToGenerate);
        InvokeRepeating(nameof(InvokeFuel), _powerupManagerModel.timeToinstantiateFuel, _powerupManagerModel.timeToinstantiateFuel);
    }
    public void OnWinLevel(EventsManager.GameEventsArgs args)
    {
        StopAllCoroutines();
        CancelInvoke();
        RemoveAllPowerups();

    }
    public void OnLoseLevel (EventsManager.GameEventsArgs args)
    {
        StopAllCoroutines();
        CancelInvoke();
        RemoveAllPowerups();
    }
    public void RemoveAllPowerups()
    {
        foreach (var item in _powerupManagerModel.powerUpsGameObjets)
        {
            ObjectPoolingMaster.instance.DisactiveObject(item);

        }
    }
}
