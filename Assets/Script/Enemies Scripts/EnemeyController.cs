﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
using System;
using UnityEngine.Events;
using Pathfinding;
public class EnemeyController : MonoBehaviour
{
    [System.Serializable]
    public class EnemeyModel
    {
        public float timeToHitBase;
        public float hitObjectSpeed;
    }
    [SerializeField]
    public EnemeyModel _model;

    public bool isAlive;
    public float changeColorTime;
    public Transform target;


    public AIPath _path;


    private bool activeHitBase;
    private Rigidbody2D rigidBody;
    private float updatedTime;
    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Base").GetComponent<Transform>();
        rigidBody = GetComponent<Rigidbody2D>();
    }
    void OnEnable()
    {
        DefaultState();
        EventsManager.OnEnemeyDoubleHitAction += DoubleHitEnemey;
        //EventsManager._onEnemyHit += EnemeyDie;
    }
    private void OnDisable()
    {
        EventsManager.OnEnemeyDoubleHitAction -= DoubleHitEnemey;

    }
    // Use this for initialization
    void Start()
    {
        _path = GetComponent<AIPath>();
        GetComponent<AIDestinationSetter>().target = target;
    }

    // Update is called once per frame
    void Update()
    {
        _ = UpdateTimer();

        if (isAlive)
        {
            if (updatedTime < _model.timeToHitBase / 2)
            {
                // UpdateTimer();
                ChangeColorToHit(changeColorTime);
                GetComponent<Animator>().SetBool("BeforHitTheBase", true);

            }
            if (updatedTime < _model.timeToHitBase / 2)
            {
                ///Play Hit Animation
              //  UpdateTimer();
            }
            if (updatedTime <= 0)
            {
                GetComponent<Animator>().SetBool("BeforHitTheBase", false);
                //  if (activeHitBase) HitTheBase();                //Hit the base call Included in the Animation 
                Invoke(nameof(HitTheBase), 0);
                //   HitTheBase();

                /* if ((Mathf.CeilToInt(transform.position.magnitude) == Mathf.CeilToInt( target.position.magnitude)))
                 {

                 }*/
            }
        }
        else
        {
            OutAreaHandler();
        }

    }
    public void ActiveHitBase()
    {
        activeHitBase = true;
    }
    public void HitTheBase()
    {
        if (isAlive)
        {
            isAlive = true;
            rigidBody.constraints = RigidbodyConstraints2D.None;
            _path.canMove = true;

            // rigidBody.MovePosition(Vector3.LerpUnclamped(transform.position, target.position, Time.deltaTime * _model.hitObjectSpeed));
           
        }

    }
    public float UpdateTimer()
    {
        if (Mathf.RoundToInt(updatedTime) == 0)
        {
            updatedTime = 0;

            return updatedTime;
        }
        else
        {
            updatedTime -= Time.deltaTime;
            return updatedTime;
        }

    }
    public void ChangeColorToHit(float val)
    {
        GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, Color.red, Time.deltaTime / val);
    }
    public void EnemeyDie()
    {
        CancelInvoke();
        isAlive = false;
        // GetComponent<EnemyAI>().enabled = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        _path.enabled = false;
        // GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        //EventsManager._callChangeGameBackColor();
        // GetComponent<SpriteRenderer>().material.color = Color.yellow;
        //EventsManager._callOnChangeActiveState(gameObject, 1, false);
        //  gameObject.SetActive(false);
        // ScoreManager.Score++;
    }
    public void DefaultState()
    {
        CancelInvoke();
        updatedTime = _model.timeToHitBase;
        Animator anim = GetComponent<Animator>();
        anim.enabled = false;
        GetComponent<Animator>().SetBool("BeforHitTheBase", false);
        isAlive = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        GetComponent<SpriteRenderer>().color = Color.white;
        rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;

        activeHitBase = false;
        anim.enabled = true;

        //  GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
    }
    public void OutAreaHandler()
    {
        if (transform.position.y > GameManager.screenMaxHeight || transform.position.y < -GameManager.screenMaxHeight || transform.position.x > GameManager.screenMaxWidth || transform.position.x < -GameManager.screenMaxWidth)
        {
            Debug.Log("out of the area");
            gameObject.SetActive(false);
            EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
            args._particleID = 6;
            args._particlePosition = this.transform;
            EventsManager.OnPlayParticleAction.Invoke(args);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //// Check If The Enemy HitThe Base 
        if (collision.CompareTag("Base") || collision.CompareTag("BaseL1") || collision.CompareTag("BaseL2") || collision.CompareTag("BaseL3"))
        {


            if (isAlive)
            {
                EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
                args._particleID = 0;
                args._particlePosition = this.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);
                Handheld.Vibrate();
                EventsManager.OnHitBaseAction.Invoke();
            }
            else
            {
                EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
                args._particleID = 6;
                args._particlePosition = this.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);
            }
            this.gameObject.SetActive(false);
            activeHitBase = false;
            this.transform.rotation = Quaternion.Euler(0, 0, 0);


        }
        if (collision.name == "Hammer")
        {
           // GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        }
    }
    public void DoubleHitEnemey()
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._particleID = 6;
        args._particlePosition = this.transform;
        EventsManager.OnPlayParticleAction.Invoke(args);
        gameObject.SetActive(false);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
            args._particleID = 3;
            args._particlePosition = this.transform;
            EventsManager.OnPlayParticleAction.Invoke(args);

            args._particlePosition = collision.transform;

            EventsManager.OnPlayParticleAction.Invoke(args);
            args._InGameText = "Double Hit";
            EventsManager.ShowInGameTextAction.Invoke(args);
            gameObject.SetActive(false);
            collision.gameObject.SetActive(false);
        }
        else
        {
            return;
        }
       // Debug.Log(collision.gameObject + "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");

    }

}
