﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {
    public Transform[] enemyPath;
    public float speed;
    public bool isMagnetActive;
    public GameObject axis;

    void Start () {
        axis = GameObject.FindGameObjectWithTag("Axis");
	}
    void Update () {
       // EnemyMoverAI(10);
        Fire();
	}
    int pathNum;
    public bool changePath;
    int hopcounter = 0;
    public void EnemyMoverAI(int hopcount)
    {
        if (isMagnetActive)
        {
            transform.position = Vector2.Lerp(transform.position, axis.transform.position, speed * Time.deltaTime);
        }
        else
        {
            if (changePath)
            {
                pathNum = Random.Range(0, enemyPath.Length);
            }
            else
            {
                if (Mathf.Abs( Mathf.Round( transform.position.magnitude))==Mathf.Abs(Mathf.Round(enemyPath[pathNum].transform.position.magnitude)))
                {

                    changePath = true;
                    hopcounter++;
                    return;
                }
            }
            if (hopcounter == hopcount)
            {
                transform.position=Vector2.MoveTowards(transform.position,GameObject.FindGameObjectWithTag("Base").transform.position,0f);
               // Destroy(this.gameObject, 5f);
               
                return;
            }
            else
            {
                changePath = false;
                Vector2 path = new Vector2(enemyPath[pathNum].transform.position.x, enemyPath[pathNum].transform.position.y);
                transform.position = Vector2.Lerp(transform.position, path, speed * Time.deltaTime);
                speed = speed + Time.deltaTime;
                if (speed > 3)
                {
                    speed = 1;
                }
            }

        }
    }
    public GameObject fireBullet;
    public float bulletShootTime = 5;
    public bool canshoot;
    public void Fire()
    {
        if (canshoot)
        {
            this.transform.Rotate(new Vector3(0, 0, 360 * Time.deltaTime));
            if ((int)bulletShootTime == 0)
            {
                GameObject bulllet=  Instantiate(fireBullet, this.transform.position, this.transform.rotation)as GameObject;
                bulletShootTime = 2;
                Destroy(bulllet, 3);
            }
            else
            {
                bulletShootTime -= Time.deltaTime;
            }
        }
    }
    public IEnumerator ActiveMagnetModeForTime(float time)
    {
        isMagnetActive = true;
        yield return new WaitForSecondsRealtime(time);
        isMagnetActive = false;
    }
}
