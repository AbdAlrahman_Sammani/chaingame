﻿using GameSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Decide Enemy Type To Instantiate and put theme in List
/// </summary>
public class EnemyManager : MonoBehaviour
{
    [System.Serializable]
    public class EnemeyManagerModel
    {
        public GameObject[] enemy;
        public float time;
        public int maxEnemiesNumber;
        // public List<GameObject> enemyList;
    }
    public EnemeyManagerModel _enemeyManagerModel;

    GridGenerator gridGenerator;

    private void OnEnable()
    {
        EventsManager.OnStartNewLevelActions += StartEnemeyManager;
        EventsManager.OnWinLevelActions += OnWinLevel;
        EventsManager.OnLoseLevelActions += OnLoseLevel;
    }
    private void OnDisable()
    {
        EventsManager.OnStartNewLevelActions -= StartEnemeyManager;
        EventsManager.OnWinLevelActions -= OnWinLevel;
        EventsManager.OnLoseLevelActions -= OnLoseLevel;
    }
    void Awake()
    {

    }
    private void Start()
    {
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            RemoveAllEnemies();
        }
    }


    public void EnemyInstantiator()
    {
       // if (GameManager.gameState == GameState.InGameNoBoss)
     //   {
            int rand = Random.Range(0, _enemeyManagerModel.enemy.Length);
            GameObject objectRef;
            //  Vector3 spawnPoint = gridGenerator.gridPositionsList[Random.Range(0, gridGenerator.gridPositionsList.Count - 1)];
            var unusedList = GameManager.Instance.unUsedListPos;
            var spawnPoint = gridGenerator.GetRandomPosition3(unusedList);
            ObjectPoolingMaster.instance.ReuseGameObject(_enemeyManagerModel.enemy[rand], spawnPoint.position, Quaternion.identity, out objectRef);
            spawnPoint.state = true;
            FindObjectOfType<AiPathController>().UpdatePath();
    //    }
     //   else
     //   {
      //      return;
       // }

    }

    public void UpdateEnemyPool()
    {
        foreach (var item in _enemeyManagerModel.enemy)
        {
            ObjectPoolingMaster.instance.UpdatePool(item, _enemeyManagerModel.maxEnemiesNumber);
        }
    }
    public void StartEnemeyManager(EventsManager.GameEventsArgs args)
    {
        _enemeyManagerModel = args._EnemyManagerModel;
        foreach (var item in _enemeyManagerModel.enemy)
        {
            ObjectPoolingMaster.instance.Createpool(item, _enemeyManagerModel.maxEnemiesNumber);
        }
        gridGenerator = FindObjectOfType<GridGenerator>();
        InvokeStartMethods();
    }
    public void InvokeStartMethods()
    {
        CancelInvoke();
        InvokeRepeating(nameof(EnemyInstantiator),_enemeyManagerModel.time, _enemeyManagerModel.time);
    }

    public void SetEnemeyManagerAttribute(EnemeyManagerModel _model)
    {
        _enemeyManagerModel = _model;
        InvokeStartMethods();
    }
    public void OnWinLevel(EventsManager.GameEventsArgs args)
    {
        CancelInvoke();
        RemoveAllEnemies();
    }
    public void OnLoseLevel(EventsManager.GameEventsArgs args)
    {
        CancelInvoke();
        RemoveAllEnemies();
    }
    public void RemoveAllEnemies()
    {
        //foreach (var item in FindObjectsOfType<EnemeyController>())
        //{
        //    Destroy(item.gameObject);
        //}
        foreach (var item in _enemeyManagerModel.enemy)
        {
            ObjectPoolingMaster.instance.DisactiveObject(item);
        }
        {

        }
    }
}
 