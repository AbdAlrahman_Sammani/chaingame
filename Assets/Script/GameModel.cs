﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameModel 
{
    public float fuelAmount;
    public int scoreAmount;
}
