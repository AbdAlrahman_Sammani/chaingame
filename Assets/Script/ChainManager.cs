﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public class ChainManager : MonoBehaviour
{
    [System.Serializable]
    public class ChainModel
    {
        public float speed;
        public float fuleAmount;
    }

    public ChainModel _chainModel;
    public HingeJoint2D axis_hingeJoint;
    public GameObject hammer;
  //  public GameObject chain;
    public JointMotor2D motor;
    public ParticleSystem chainTailParticle;
    private TrailRenderer trail;

    public  bool maceReleased;
    private int downPortal;
    public bool canMove;
    private void OnEnable()
    {
        EventsManager.OnStartNewLevelActions += OnStartNewLevel;
        EventsManager.OnTouchScreenAction += MoveTheChain;
        EventsManager.OnChangeHammerSizeAction += ChangeHammerSize;
        EventsManager.OnWinLevelActions += OnLevelWin;
        EventsManager.OnLoseLevelActions += OnLevelLose;
       
    }
    private void OnDisable()
    {
        EventsManager.OnStartNewLevelActions -= OnStartNewLevel;
        EventsManager.OnTouchScreenAction -= MoveTheChain;
        EventsManager.OnChangeHammerSizeAction -= ChangeHammerSize;
        EventsManager.OnWinLevelActions -= OnLevelWin;
        EventsManager.OnLoseLevelActions -= OnLevelLose;

    }

    void Start()
    {
       // chain = this.gameObject;
        axis_hingeJoint = GameObject.FindGameObjectWithTag("Axis").GetComponent<HingeJoint2D>();
        motor = axis_hingeJoint.motor;
        axis_hingeJoint.useMotor = false;
        trail = GetComponent<TrailRenderer>();
    }
   public float rot;
    void Update()
    {
        AreaHandler();
        FuelHandler();
        
    }

    public void MoveTheChain()
    {
        if (!canMove) return;
        
        downPortal = 0;
        if (axis_hingeJoint.autoConfigureConnectedAnchor != true)
        {
            axis_hingeJoint.autoConfigureConnectedAnchor = true;
        }
        /// set Axis free or use it
        
        axis_hingeJoint.useMotor = !axis_hingeJoint.useMotor;
        if (axis_hingeJoint.useMotor == false)
        {
            maceReleased = true;
            EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
            args._soundID = 2;
            EventsManager.OnSoundPlayCallAction(args);
            axis_hingeJoint.enabled = false;
            chainTailParticle.Stop();
            Time.timeScale = 1;
        }
        else
        {

           // HoldTheMace();
            //   maceReleased = false;
            // axis_hingeJoint.enabled = true;
            //  motor.motorSpeed = speed;
            //   chainTailParticle.Play();
            //    axis_hingeJoint.motor = motor;

        }
    }
    public void HoldTheMace()
    {
        if (maceReleased == true)
        {
                axis_hingeJoint.useMotor = !axis_hingeJoint.useMotor;
                maceReleased = false;
                axis_hingeJoint.enabled = true;
                motor.motorSpeed = _chainModel.speed ;
                chainTailParticle.Play();
                axis_hingeJoint.motor = motor;
            
        }
    }
    public void AreaHandler()
    {
        if (downPortal == 2)
        {
            ///Out of Game Area
            if (transform.position.y < -GameManager.screenMaxHeight)
            {
                axis_hingeJoint.enabled = true;
                axis_hingeJoint.autoConfigureConnectedAnchor = false;
                axis_hingeJoint.connectedAnchor = new Vector2(0, 0);
                axis_hingeJoint.anchor = new Vector2(0, 0);
            }
            downPortal = 0;
        }
        //  else
        //{
        //  transform.position = new Vector2(transform.position.x, Mathf.Clamp(transform.position.y, -GameManager.screenMaxHeight, GameManager.screenMaxHeight));
        //}

        Portal();
        //end of Out Area
    }
    public void Portal()
    {
        // if(direction=="Left")

        if (transform.position.x < -GameManager.screenMaxWidth-0.1)
        {
           // trail.enabled = false;

            transform.position = new Vector2(GameManager.screenMaxWidth, transform.position.y);
        }
        else if (transform.position.x > GameManager.screenMaxWidth+0.1)
        {
           // trail.enabled = false;
            transform.position = new Vector2(-GameManager.screenMaxWidth, transform.position.y);
        }
        else
        {
            if (trail.enabled == false)
                Invoke("EnableTrail", .2f);
        }
        if (transform.position.y < -GameManager.screenMaxHeight-0.1)
        {
            if (downPortal == 0)
            {
                downPortal++;
                trail.enabled = false;

                transform.position = new Vector2(transform.position.x, GameManager.screenMaxHeight);
            }
            else
            {
                MoveAxisToCenter();

                downPortal = 0;

            }
        }
        else if (transform.position.y > GameManager.screenMaxHeight+0.1)
        {


            trail.enabled = false;
            transform.position = new Vector2(transform.position.x, -GameManager.screenMaxHeight);

        }
        else
        {
            if (trail.enabled == false)
                Invoke("EnableTrail", .2f);
        }


    }

    private void MoveAxisToCenter()
    {
        Debug.Log("Mo");
        axis_hingeJoint.enabled = true;
        axis_hingeJoint.autoConfigureConnectedAnchor = false;
        axis_hingeJoint.connectedAnchor = new Vector2(0, 0);
        axis_hingeJoint.anchor = new Vector2(0, 0);
        axis_hingeJoint.useMotor = true;
        chainTailParticle.Play();
    }

    public void EnableTrail()
    {
       // trail.enabled = true;

    }
    public void ObjetState(bool state, GameObject go)
    {
        go.SetActive(state);
    }

    public IEnumerator ChangeHammerSizeForTimeCor(EventsManager.GameEventsArgs args)
    {
        hammer.transform.localScale = new Vector2(args._hammerSize, args._hammerSize);
        yield return new WaitForSeconds(args._powerupTime);
        hammer.transform.localScale = new Vector2(1.5f, 1.5f);
    }
    public void ChangeHammerSize(EventsManager.GameEventsArgs args)
    {
        StartCoroutine(ChangeHammerSizeForTimeCor(args));
    }
    public IEnumerator ReviveTheChain()
    {
        //Time.timeScale = .5f;
        yield return new WaitForSecondsRealtime(10);
        axis_hingeJoint.useMotor = true;
        _chainModel.fuleAmount = 100;
        // Time.timeScale = 1;
    }
    public void FuelHandler()
    {
        if (axis_hingeJoint.useMotor == true)
        {
            _chainModel.fuleAmount -= Time.deltaTime;
            // fuelHud.text = ((int)DataContainer.instance.fuelAmount).ToString();
            if ((int)_chainModel.fuleAmount == 0)
            {
                EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
                args._soundID = 3;
                EventsManager.OnSoundPlayCallAction(args);
                axis_hingeJoint.useMotor = false;
                StartCoroutine(ReviveTheChain());
            }
        }
    }

    public void OnStartNewLevel(EventsManager.GameEventsArgs args)
    {
        axis_hingeJoint.useMotor = true;
        _chainModel.fuleAmount = DataContainer.instance.fuelAmount;
        canMove = true;

        // axis_hingeJoint.transform.position = Vector3.zero;
    }
    public void OnLevelWin(EventsManager.GameEventsArgs args)
    {
       // axis_hingeJoint.useMotor = false;
      //  axis_hingeJoint.transform.position = Vector3.zero;
        MoveAxisToCenter();
        axis_hingeJoint.useMotor = false;
        _chainModel.fuleAmount = 0;
        canMove = false;

    }
    public void OnLevelLose(EventsManager.GameEventsArgs args)
    {
      //  axis_hingeJoint.useMotor = false;
      //  axis_hingeJoint.transform.position = Vector3.zero;
        MoveAxisToCenter();
        axis_hingeJoint.useMotor = false;
        _chainModel.fuleAmount = 0;
        canMove = false;


    }



}
