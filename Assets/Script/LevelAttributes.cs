﻿using GameSystem;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class LevelAttributes : MonoBehaviour
{
  
    [System.Serializable]
    public class LevelModel
    {
        [SerializeField]
        public  EnemyManager.EnemeyManagerModel _enemeyManagerModel;
        [SerializeField]
        public  PowerUpsManager.PowerupManagerModel _powerupModel;
        [SerializeField]
        public TheBase.BaseHealthModel _baseHealthModel;
        [SerializeField]
        public TheBase.BaseShapeModel _baseShapeModel;
        public int scoreToWin;
        public GameObject boss;
    }
    public LevelModel _levelModel;
    // Start is called before the first frame update
    private void OnEnable()
    {
        CallStartLevel();
    }
    private void OnDisable()
    {
        
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CallStartLevel()
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._powerUpManagerModel = _levelModel._powerupModel;
        args._EnemyManagerModel = _levelModel._enemeyManagerModel;
        args._healthModel = _levelModel._baseHealthModel;
        StartLevel(args);
    }
    public void StartLevel(EventsManager.GameEventsArgs args)
    {
        EventsManager.OnStartNewLevelActions.Invoke(args);
       
      
    }
    
}
