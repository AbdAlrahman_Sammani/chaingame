﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
using System;
/// <summary>
///  this script decide what happend when the hammer hit object
/// </summary>
public class HitObjectManager : MonoBehaviour {
    public GameObject explosion;
    public GameObject Hammer;
   // private GameManager gameManager;
    public string[] hitTag;
    public EnemyAI enemyAI;
    //data accessor
    DataContainer gameData;
    void Start() {
       // gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update() {
      
    }

    /// <summary>
    /// On Hit Power up 
    /// </summary>
    /// <param name="collision"></param>
    public void OnTriggerEnter2D(Collider2D collision)
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._soundID = 1;
       
        switch (collision.tag)
        {
            case ("FuelPowerUp"):
               
                EventsManager.OnSoundPlayCallAction.Invoke(args);
                args._particleID = 9;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);
                args._powerupType = PowerUps.Fuel;
                EventsManager.OnPowerupCallAction(args);
                ///do some animation here
                collision.gameObject.SetActive(false);
                break;
            case ("MagnetPowerUp"):
                EventsManager.OnSoundPlayCallAction(args);
                args._particleID = 9;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);
                args._powerupType = PowerUps.Magnet;
                EventsManager.OnPowerupCallAction(args);
                collision.gameObject.SetActive(false);
                break;
            case ("LargePowerUp"):
                EventsManager.OnSoundPlayCallAction(args);
                args._particleID = 2;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);

                args._powerupType = PowerUps.Large;
                EventsManager.OnPowerupCallAction(args);
                collision.gameObject.SetActive(false);
                break;
            case ("WavePowerUp"):
                EventsManager.OnSoundPlayCallAction(args);
                args._particleID = 9;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);

                args._powerupType = PowerUps.Wave;
                EventsManager.OnPowerupCallAction(args);
                collision.gameObject.SetActive(false);
                break;
            case ("SlowPowerUp"):
                EventsManager.OnSoundPlayCallAction(args);
                args._particleID = 13;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);

                args._powerupType = PowerUps.Slow;
                EventsManager.OnPowerupCallAction(args);
                collision.gameObject.SetActive(false);
                break;
            case ("BombPowerUp"):
                EventsManager.OnSoundPlayCallAction(args);
                args._particleID = 2;
                args._particlePosition = collision.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);

                args._powerupType = PowerUps.Bomb;
                EventsManager.OnPowerupCallAction(args);
                collision.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                collision.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
                break;
            case ("Enemy"):
                collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// On Hit Enemey
    /// </summary>
    /// <param name="collision"></param>
    public void OnCollisionEnter2D(Collision2D collision)
    {
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        switch (collision.gameObject.tag) {
            case ("Enemy"):
                if (collision.gameObject.GetComponent<EnemeyController>().isAlive == false)
                {

                  //  collision.gameObject.GetComponent<EnemeyController>().DoubleHitEnemey();
                    //args._InGameText = "Double Kick";
                    //EventsManager.ShowInGameTextAction.Invoke(args);
                }
                else
                {
                    collision.gameObject.GetComponent<EnemeyController>().EnemeyDie();

                    //collision.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(rig.velocity,collision.contacts[0].point,ForceMode2D.Force);
                    // explosion.SetActive(false);
                    // explosion.SetActive(true);
                    // ScoreManager.Score++;
                    int score = DataContainer.instance.scoreAmount++;
                    args._ScoreUpdateValue = score.ToString();
                    EventsManager.ScoreUpdaterAction.Invoke(args);
                    EventsManager.ChageBackgroundAction.Invoke();

                }
                args._soundID = 0;
                EventsManager.OnSoundPlayCallAction(args);

                args._particleID = 1;
                args._particlePosition = this.transform;
                EventsManager.OnPlayParticleAction.Invoke(args);
                break;
            default:
                break;
        }
    }
    //public IEnumerator ToggleCollider(GameObject go)
    //{
    //    go.GetComponent<BoxCollider2D>().isTrigger = true;
    //    yield return new WaitForSecondsRealtime(0.5f);
    //    go.GetComponent<BoxCollider2D>().isTrigger = false;

    //}
    //public IEnumerator TimeScaling(int time)
    //{
    //    StopCoroutine("TimeScaling");
    //    Time.timeScale = 1;
    //    yield return new WaitForSecondsRealtime(time);
    //    Time.timeScale = 0.5f;

    //}


}

