﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    [System.Serializable]
    public class GameData
    {
        public float fuelAmount;
        public int scoreAmount;
    }
[CreateAssetMenu()]
public class DataObject : ScriptableObject
{
    public DataContainer GameData;
}
