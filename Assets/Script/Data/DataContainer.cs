﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public class DataContainer : MonoBehaviour {
    [SerializeField]
    private GameModel _GameModel;
    public float fuelAmount;
    public int scoreAmount;
    static DataContainer _instance;
    
    public static DataContainer instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DataContainer>();
            }
            return _instance;
        }
    }
    private void OnEnable()
    {
        EventsManager.OnDataUpdateAction += UpdateData;
        fuelAmount = _GameModel.fuelAmount;
        scoreAmount = _GameModel.scoreAmount;
    }
    public void UpdateData()
    {
        _GameModel.fuelAmount = fuelAmount;
        _GameModel.scoreAmount = scoreAmount;
    }
    private void FixedUpdate()
    {
        UpdateData();
    }
}
