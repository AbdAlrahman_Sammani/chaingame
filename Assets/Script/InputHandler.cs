﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem;
public class InputHandler : MonoBehaviour {
    bool disableInput;
    public ChainManager chainManager;
    public float slowMotionScale;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        InputForChain();
	}
    private int upPressCount;
    public void InputForChain()
    {
       if(GameManager.gameState==GameState.InGameNoBoss || GameManager.gameState==GameState.InGameFightBoss || GameManager.gameState==GameState.Endless)
        if (Input.GetMouseButton(0))
        {
                if(upPressCount==2)
                Time.timeScale = slowMotionScale;
                
        }
        if (Input.GetMouseButtonDown(0))
        {
              chainManager.HoldTheMace();

        }
        if (Input.GetMouseButtonUp(0))
        {
            
            if (upPressCount == 2)
            {
                EventsManager.OnTouchScreenAction();
                upPressCount = 0;
            }
            Time.timeScale = 1;
            upPressCount++;
            // EventsManager._callOnTouchScreen();
            // Time.timeScale = 0.5f;
            //Handle Slow Motion
        }
        if (Input.GetMouseButtonUp(0))
        {
           // Time.timeScale = 1f;
           //Exit Slow Motion
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
    public IEnumerator ObjectDisabler(bool en, float time, GameObject go)
    {
        disableInput = true;
        go.SetActive(en);
        yield return new WaitForSeconds(time);
        go.SetActive(true);
        disableInput = false;

    }
}
