﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameSystem;
public class InGameUIHandler : MonoBehaviour {
    public Image fuelAmountImage;
    public TextMeshProUGUI scoreText;
	// Use this for initialization
	void Start () {
		
	}
    private void OnEnable()
    {
        EventsManager.ScoreUpdaterAction += UpdateScoreText;
    }
    private void OnDisable()
    {
        EventsManager.ScoreUpdaterAction -= UpdateScoreText;

    }

    // Update is called once per frame
    void Update () {
        UpdateFuelAmount();
	}
    public void UpdateFuelAmount()
    {
        fuelAmountImage.fillAmount = DataContainer.instance.fuelAmount / 100;
    }
    public void UpdateScoreText(EventsManager.GameEventsArgs args)
    {
        scoreText.text = "Score : " + args._ScoreUpdateValue;
    }

}
