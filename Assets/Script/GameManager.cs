﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSystem;
using UnityEngine.SceneManagement;

public enum GameState { Loading, StartScreen, InGameNoBoss, Endless, InGameFightBoss, Lose, Win }
public class GameManager : MonoBehaviour
{
    public class GameManagerModel
    {

    }
    public static GameState gameState = new GameState();
    public HingeJoint2D axis;
    public Image backgroundMaterial;
    public Color currentColor, desiredColor;
    public bool changeColor;
    public PowerUpsManager powerUpManager;
    /// screen Area Parameters
    public static float screenMaxWidth;
    public static float screenMaxHeight;
    //Spawning Area
    public static float spawningAreaWidth;
    public static float spawningAreaHeight;
    public static float spawningAreaMinHeight;

    public List<Vector2> unUsedListPos;
    public static GameManager Instance;

    public float minDistanceFromBase;

    public List<GameObject> allLevelsManagers;
    private void OnEnable()
    {
        // EventsManager.OnChangeActiveStateAction += ChangeActiveGameobject;
        EventsManager.ChageBackgroundAction += ChangeColorState;
        EventsManager.ChageBackgroundAction += BackGroundColorChanger;
        EventsManager.OnHitBaseAction += AnimateCamera;

        EventsManager.OnWinLevelActions += OnLevelWin;
        EventsManager.OnGameOverAction += OnLevelLose;
    }
    private void OnDisable()
    {
        //  EventsManager.OnChangeActiveStateAction -= ChangeActiveGameobject;
        EventsManager.ChageBackgroundAction -= ChangeColorState;
        EventsManager.ChageBackgroundAction -= BackGroundColorChanger;
        EventsManager.OnHitBaseAction -= AnimateCamera;

        EventsManager.OnWinLevelActions -= OnLevelWin;

        EventsManager.OnGameOverAction -= OnLevelLose;
    }
    void Start()
    {
        Instance = this;
        gameState = GameState.StartScreen;
        // DontDestroyOnLoad(this.gameObject);
        CalculateScreenArea();
        //  CalculateSpawningArea();
        Invoke(nameof(GetUnusedPositions), 1);
    }

    void Update()
    {
        /// BackGroundColorChanger();
        // FuelHandler();
    }
    /// <summary>
    /// Change Background Color Automaticaly By The Time 
    /// </summary>
    public void BackGroundColorChanger()
    {

        if (currentColor != desiredColor)
        {
            backgroundMaterial.material.color = Color.Lerp(currentColor, desiredColor, Time.deltaTime * 10);
            currentColor = backgroundMaterial.material.color;
        }
        else
        {
            if (changeColor)
            {
                float H, S, V;
                Color.RGBToHSV(currentColor, out H, out S, out V);
                float h = Random.Range(.1f, .9f);
                desiredColor = Color.HSVToRGB(h, S, V);

                if (desiredColor == Color.black)
                    return;
                else
                    changeColor = false;
            }
        }
    }
    public void ChangeColorState()
    {
        Debug.Log("CHange COlor Called");
        if (changeColor == false)
            changeColor = true;
        else return;
    }

    //    public Text fuelHud;

    //public void FuelHandler()
    //{
    //    if (axis.useMotor == true)
    //    {
    //      DataContainer.instance.fuelAmount -=  Time.deltaTime;
    //        fuelHud.text = ((int)DataContainer.instance.fuelAmount).ToString();
    //        if ((int)DataContainer.instance.fuelAmount == 0)
    //        {
    //            EventsManager._onSoundPlayCall(3);
    //            axis.useMotor = false;
    //            StartCoroutine(ReviveTheChain());
    //        }
    //    }
    //}
    //public IEnumerator ReviveTheChain()
    //{
    //    //Time.timeScale = .5f;
    //    yield return new WaitForSecondsRealtime(10);
    //    axis.useMotor = true;
    //    DataContainer.instance.fuelAmount = 100;
    //   // Time.timeScale = 1;
    //}
    public void CalculateScreenArea()
    {
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height);
        Vector3 moveRange = Camera.main.ScreenToWorldPoint(upperCorner);
        screenMaxWidth = moveRange.x;
        screenMaxHeight = moveRange.y;
        print("Width" + screenMaxWidth + ",,Height=" + screenMaxHeight);
    }
    /* public IEnumerator ChangeGameObjectActiveState(EventsManager.GameEventsArgs args)
     {
         yield return new WaitForSecondsRealtime(args._changeActiveStateTime);
         //        Debug.Log(ob.name);
         args._changeActiveStateGameObject.SetActive(args._changeActiveState_bool);
     }
     public void ChangeActiveGameobject(EventsManager.GameEventsArgs args)
     {
         StartCoroutine(ChangeGameObjectActiveState(args));
     }*/

    public void CalculateSpawningArea()
    {
        spawningAreaHeight = screenMaxHeight - 1;
        spawningAreaWidth = screenMaxWidth - 1;
        float baseHeight = GameObject.FindGameObjectWithTag("BaseL1").GetComponent<SpriteRenderer>().bounds.extents.y;
        print(baseHeight);
        spawningAreaMinHeight = -screenMaxHeight + baseHeight + 1;
    }

    public void OnLevelLose(EventsManager.GameEventsArgs args)
    {
        ShowInGameItems(false);

        gameState = GameState.StartScreen;
    }
    public void OnLevelWin(EventsManager.GameEventsArgs args)
    {
        ShowInGameItems(false);

        gameState = GameState.StartScreen;
    }
    public void StartGame()
    {
        ShowInGameItems(true);
        gameState = GameState.InGameNoBoss;
    }
    public void StartNewLevel(int levelID)
    {

        Instantiate(allLevelsManagers[levelID], transform.position, Quaternion.identity);
        StartGame();
        EventsManager.OnStartGameAction.Invoke(new EventsManager.GameEventsArgs());
    }
    public void ShowInGameItems(bool state)
    {

        // GetComponent<EnemyManager>().enabled = state;
        // powerUpManager.enabled = state;
        EventsManager.GameEventsArgs args = new EventsManager.GameEventsArgs();
        args._startState = state;
        //  EventsManager.OnStartGameAction.Invoke(args);
        // axis.GetComponent<ChainManager>().enabled = state;
        if (state == false)
        {
            // axis.useMotor = false;
            //  axis.transform.position = Vector3.zero;
            foreach (var item in GetComponentsInChildren<Transform>())
            {
              //  if (item.gameObject.CompareTag("GameManager")) continue;
              //  item.gameObject.SetActive(false);
            }
        }
        if (state == true)
        {
            //  axis.useMotor = true;
        }
    }
    public void AnimateCamera()
    {
        Camera.main.GetComponent<Animator>().SetTrigger("Shake");
    }
    public void GetUnusedPositions()
    {
        var baseGameObject = FindObjectOfType<TheBase>().gameObject;
        List<Vector2> result = new List<Vector2>();
        GridGenerator grid = GetComponent<GridGenerator>();
        var screenSize = grid.screenSizeInworldSpace;
        foreach (var item in grid.positonsList)
        {
            if ((Vector2.Distance(item.position, baseGameObject.transform.position) <= minDistanceFromBase))
            {
                result.Add(item.reference);
            }


            Debug.Log("X==" + item.position.x);

            if (item.position.x + screenSize.x <= 1f)
            {
                Debug.Log("ADD++++++++++++++++");
                result.Add(item.reference);
            }
            if (screenSize.x - item.position.x > 0 && screenSize.x - item.position.x <= 1f)
            {
                Debug.Log("ADD++++++++++++++++");
                result.Add(item.reference);
            }
            if (item.position.y + screenSize.y <= 1f)
            {
                Debug.Log("ADD++++++++++++++++");
                result.Add(item.reference);
            }
            //if (item.position.y - screensize.y <= 1)
            //{
            //    result.add(item.reference);
            //}
        }
        unUsedListPos = result;

        // return unUsedListPos;

    }



}


