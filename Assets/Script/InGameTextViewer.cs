﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GameSystem;
public class InGameTextViewer : MonoBehaviour {
    public GameObject ingameTestGameObject;
     TextMeshProUGUI inGameText;
    public void OnEnable()
    {
        EventsManager.ShowInGameTextAction += ShowText;
    }
    private void OnDisable()
    {
        EventsManager.ShowInGameTextAction -= ShowText;

    }
    // Use this for initialization
    void Start () {
        inGameText = ingameTestGameObject.GetComponent<TextMeshProUGUI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ShowText(EventsManager.GameEventsArgs args)
    {
        inGameText.text = args._InGameText;
        ingameTestGameObject.SetActive(true);
        Invoke("DisableGameObject", 2f);
    }
    public void DisableGameObject()
    {
        ingameTestGameObject.SetActive(false);
    }
}
